export class ResponseModel
{
    message : string
    data : any
    token? : string

	constructor()
	{
		this.message = ''
		this.data = []
		this.token = ''
	}
}