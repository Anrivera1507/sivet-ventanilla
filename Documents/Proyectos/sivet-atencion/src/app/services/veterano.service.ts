import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { ResponseModel } from '../models/responseModel';
import { ConfigService } from './config.service';

@Injectable({
  providedIn: 'root'
})
export class VeteranoService {

  constructor(
    private _http   :    HttpClient,
    private _config :    ConfigService
  ) { }
 
  public buscar(objParam: any): Observable<any> { 
    return this._http.post<any>(environment.urlAPI+'/registros/veteranos/buscar',objParam, {'headers':this._config.headers})
    .pipe(
            map((resp: any) => { 
                return resp;
            }),
            catchError(err => {
                console.log(err)  
                return err;
            })
        )
  }
  
  public obtener(id: any): Observable<any> { 
    return this._http.post<any>(environment.urlAPI+'/registros/veteranos/obtener',id, {'headers':this._config.headers})
    .pipe(
            map((resp: any) => {
                return resp;
            }),
            catchError(err => {
                console.log(err)
                return err;
            })
        )
  }
  // /vet_opercarnet/create
  public guardarSobrevivencia(objOperCarnet: any): Observable<any> {  
    return this._http.post<any>(environment.urlAPI+'/registros/sobrevivencias/guardar', objOperCarnet, {
        'headers':this._config.headers}).pipe(
            map((resp: any) => { 
                return resp;
            }),
            catchError(err => {
                console.log(err)
                return err; 
            })
        )
  }   

  public obtenerSovrevivencia(objParam: any): Observable<any> { 
    return this._http.post<any>(environment.urlAPI+'/registros/sobrevivencias/obtener',objParam, {'headers':this._config.headers})
    .pipe(
            map((resp: any) => {
                return resp;
            }),
            catchError(err => {
                console.log(err)
                return err;
            })
        )
  }

  public getOperativoByCarnet(objOperCarnet: any): Observable<any> {   
    return this._http.post<any>(environment.urlAPI+'/vet_opercarnet/getByCarnet', objOperCarnet, {
        'headers':this._config.headers}).pipe(
            map((resp: any) => { 
                return resp;
            }),
            catchError(err => {
                console.log(err)
                return err; 
            })
        )
  }  

  public obtenerSondeo(objOperCarnet: any): Observable<any> {   
    return this._http.post<any>(environment.urlAPI+'/registros/sondeos/obtener', objOperCarnet, {
        'headers':this._config.headers}).pipe(
            map((resp: any) => { 
                return resp;
            }),
            catchError(err => {
                console.log(err)
                return err; 
            })
        )
  } 

  public obtenerListadosSondeo(): Observable<any> {   
    return this._http.get<any>(environment.urlAPI+'/configuraciones/catalogos/sondeo', {
        'headers':this._config.headers}).pipe(
            map((resp: any) => { 
                return resp;
            }),
            catchError(err => {
                console.log(err)
                return err; 
            })
        )
  }  

  public guardarSondeo(objSondeo: any): Observable<any> {  
    return this._http.post<any>(environment.urlAPI+'/registros/sondeos/guardar', objSondeo, {
        'headers':this._config.headers}).pipe(
            map((resp: any) => { 
                return resp;
            }),
            catchError(err => { 
                console.log(err)  
                return err; 
            })
        )
  } 

  public obtenerOperativo(): Observable<any> {   
    return this._http.get<any>(environment.urlAPI+'/registros/operativos/activo', {
        'headers':this._config.headers}).pipe(
            map((resp: any) => { 
                return resp;
            }),
            catchError(err => {
                console.log(err)
                return err; 
            })
        )
  }

  public obtenerCodeudores(idVeterano: number): Observable<ResponseModel> {
    return this._http.post<ResponseModel>(environment.urlAPI + '/registros/grupo-familiar/obtener', { "id" : idVeterano }, {'headers' : this._config.headers})
    .pipe(
          map((resp: ResponseModel) => {
              return resp.data;
          }),
          catchError(err => {
              console.log(err)
              return err;
          })
        )
  }
  
}
