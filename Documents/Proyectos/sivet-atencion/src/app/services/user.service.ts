import { ElementRef, Inject, Injectable, ViewChild } from '@angular/core';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { catchError, map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { header } from "./../misc/constants";
import { MenuService } from '../app.menu.service';
import { DOCUMENT } from '@angular/common';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  @ViewChild('mainIframe') iframe: ElementRef;

  constructor(
    private http: HttpClient,
    private _user: MenuService,
    @Inject(DOCUMENT) private document: HTMLDocument
  ) {

  }

  authentication(_user, _password) {
    return this.http.post(environment.urlAPI + "/seguridad/login", {"usuario": _user, "clave": _password},{'headers': header})
      .pipe(
        map((resp: any) => {
          console.log(resp.token);
          if (resp.token ) {
            localStorage.setItem('token', resp.token)
            localStorage.setItem('user', resp.data.usuario)
            localStorage.setItem('idUser', resp.data.id)
            
            return resp.data;
          } else {
            return null
          }
        }),
        catchError((err) => {
          // swal('Error.', err.error.exception.Message);
          //console.log(err);

          return err // Observable.throw(err);
        })
      );
  }

  refreshIframe(url: string, idIframe : string)
    {
      let iframe : any = this.document.getElementById(idIframe)
      if (iframe != null )
      {
        let ifr = iframe.contentWindow || iframe.contentDocument
        iframe.addEventListener('load', function(){
            setTimeout(() => {
                ifr.postMessage(JSON.stringify({token: localStorage.getItem('token'), user: localStorage.getItem('user'), idUser: localStorage.getItem('idUser')}), url);
            }, 1000);
            
        })
      }
        
    }
}
