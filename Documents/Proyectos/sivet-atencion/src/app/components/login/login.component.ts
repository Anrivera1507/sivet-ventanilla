import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuService } from 'src/app/app.menu.service';
import { UserService } from 'src/app/services/user.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(
    public _menu: MenuService,
    public router: Router,
    public _user: UserService
  ) { }

  userObj = {
    user: '',
    password: '',
    areasAtencion : []
}

  ngOnInit(): void {
  }
  
  authenticate(){
    this._user.authentication(this.userObj.user, this.userObj.password).subscribe(resp => {
      console.log(resp);
      if (resp) {
        this.router.navigate(["/"]);
        Swal.fire({
          title:'Bienvenid@!',
          text: resp.usuario,
          icon:'success',
          confirmButtonText:'Entendido',
          confirmButtonColor: 'darkgreen'
        }).then(function (result)
        {})
      } else {
          Swal.fire({
            title:'Aviso',
            text:'Usuario o contraseña inválidos',
            icon:'warning',
            confirmButtonText:'Entendido',
            confirmButtonColor: '#3085D6'
          }).then(function (result)
          {})
      }
    } );

  }
}
