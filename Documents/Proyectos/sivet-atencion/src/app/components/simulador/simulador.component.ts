import { SimuladorService } from './../../services/simulador.service';
import { CALENDER_CONFIG_ES, FormulaSimulacion_CalcularMontoInteres, FormulaSimulacion_FondoPrevencionMora, FormulaSimulacion_PagoMensual, FormulaSimulacion_PrestamoMasInteres } from './../../utils/constantes';
import { Component, OnInit } from '@angular/core';
import { CreCalculador } from 'src/app/models/cre_calculador';
import { CreSimulador } from 'src/app/models/cre_simulador';
import * as moment from 'moment';

@Component({
  selector: 'app-simulador',
  templateUrl: './simulador.component.html',
  styleUrls: ['./simulador.component.scss']
})
export class SimuladorComponent implements OnInit {

  listaValores: CreSimulador[] = [];
  calculador: CreCalculador = new CreCalculador();
  calendar_es = CALENDER_CONFIG_ES;
  configuracionCredito: any;
  porcentajeAnual: number = 0;
  lineaCreditos: any[] = [];
  lineaSeleccionada: any;
  
  cols = [
    {field: 'numPago', header: '# de pago', width: ''},
    {field: 'fechaPago', header: 'Fecha de pago', width: ''},
    {field: 'saldoInicial', header: 'Saldo inicial', width: ''},
    {field: 'cuota', header: 'Cuota', width: ''},
    {field: 'capital', header: 'Capital', width: ''},
    {field: 'interes', header: 'Interes', width: ''},
    {field: 'fpm', header: 'FPM', width: ''},
    {field: 'saldoFinal', header: 'Saldo final', width: ''},
  ]

  constructor(public _simulador: SimuladorService) { }

  ngOnInit(): void {
    //this.actualizarResumen();
    this.obtenerConfiguracion();
  }

  obtenerConfiguracion() {
    this._simulador.obtenerConfigiracionCreditos().subscribe((resp: any) => {
      if (resp != null) {
        this.configuracionCredito = resp;
        this.calculador.tasaInteres = this.configuracionCredito.porcentajeAnual;
        this.porcentajeAnual = this.configuracionCredito.porcentajeAnual * 100;
      }
    });
  }

  calcularFechaMasUnMes(indice: number) {
    let e = moment(new Date(this.calculador.fechaInicio));
    e = moment(e).add(indice, 'M');
    let fechaFull = new Date(e.toDate());
    return fechaFull;
  }

  actualizarResumen() {
    if (this.calculador.tasaInteres == 0 || this.calculador.tasaInteres == null || this.calculador.importePrestamo == 0 || this.calculador.importePrestamo == null) {
      this.calculador.numPagos = this.calculador.periodo * 12;
      this.calculador.prestamoMasInteres = 0;
      this.calculador.pagoMensual = 0; 
      this.calculador.fondoPrevencion = 0; 
      this.calculador.costoTotal = 0;
      this.listaValores = [];
    } else {
      //numero de pagos
      this.calculador.numPagos = this.calculador.periodo * 12;

      //prestamos + interes
      this.calculador.prestamoMasInteres = FormulaSimulacion_PrestamoMasInteres(this.calculador.importePrestamo, this.calculador.tasaInteres, this.calculador.periodo);

      //pago mensual
      this.calculador.pagoMensual = FormulaSimulacion_PagoMensual(this.calculador.prestamoMasInteres, this.calculador.numPagos, this.configuracionCredito.porcentajeFpm); 
      
      //fondo de prevencion de mora
      this.calculador.fondoPrevencion = FormulaSimulacion_FondoPrevencionMora(this.calculador.prestamoMasInteres, this.calculador.numPagos, this.configuracionCredito.porcentajeFpm); 

      //costo total del prestamo
      this.calculador.costoTotal = Number(this.calculador.prestamoMasInteres) + Number(this.calculador.fondoPrevencion);

      this.listarDetalles();
    }
    
  }

  validarTecla(event) {
    if (event.keyCode == 13) {
      this.actualizarResumen();
    }
  }

  listarDetalles() {
    this.listaValores = [];
    let acumuladorInicial = this.calculador.costoTotal;
    let acumuladorFinal = Number(this.calculador.costoTotal) - Number(this.calculador.pagoMensual);
    let cuota = this.calculador.pagoMensual;
    let capital = Number(this.calculador.importePrestamo)/Number(this.calculador.numPagos);
    let interes = FormulaSimulacion_CalcularMontoInteres(Number(this.calculador.importePrestamo), Number(this.calculador.numPagos), Number(this.calculador.tasaInteres));
    let fpm = (Number(this.calculador.fondoPrevencion) / Number(this.calculador.numPagos));
    for(let i = 0; i < this.calculador.numPagos; i++) {
      let objDetalle: CreSimulador = new CreSimulador();
      objDetalle.numPago = i + 1;
      objDetalle.fechaPago = this.calcularFechaMasUnMes(i + 1);
      if (i == 0) {
        objDetalle.saldoInicial = acumuladorInicial;
        objDetalle.saldoFinal = acumuladorFinal;
        
      } else {
        acumuladorInicial = acumuladorInicial - cuota;
        acumuladorFinal = acumuladorFinal - cuota;
        objDetalle.saldoInicial = acumuladorInicial;
        objDetalle.saldoFinal = acumuladorFinal;
      }
      objDetalle.cuota = cuota;
      objDetalle.capital = capital;
      objDetalle.interes = interes;
      objDetalle.fpm = fpm;

      this.listaValores.push(objDetalle);
    }

    this.calculador.pagoMensual = Number(this.calculador.pagoMensual.toFixed(2));
    this.calculador.costoTotal = Number(this.calculador.costoTotal.toFixed(2));
    this.calculador.fondoPrevencion = Number(this.calculador.fondoPrevencion.toFixed(2));
    this.calculador.prestamoMasInteres = Number(this.calculador.prestamoMasInteres.toFixed(2));
  }

}
