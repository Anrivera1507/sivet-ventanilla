import { Component, OnInit, AfterViewInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { PrimeNGConfig } from 'primeng/api';
import { PrimeTemplate } from 'primeng/api';
import { PrimeIcons } from 'primeng/api';
import { UserService } from 'src/app/services/user.service';
import { environment } from 'src/environments/environment';
@Component({
  selector: 'app-dashboard-atencion',
  templateUrl: './dashboard-atencion.component.html',
  styleUrls: ['./dashboard-atencion.component.scss']
})
export class DashboardAtencionComponent implements OnInit, AfterViewInit {

  constructor(private sanitizer: DomSanitizer,private _user : UserService)
   { }


  // urlRegistro =  environment.urlRegistro;
  urlRegistro: any = ''
  urlBecas: any = ''
  urlCreditos: any = ''
  urlPagos: any = ''



  ngOnInit(
  ): void {
    this.urlPagos = this.sanitizer.bypassSecurityTrustResourceUrl(environment.urlPagos)
    this.urlBecas = this.sanitizer.bypassSecurityTrustResourceUrl(environment.urlBecas)
    this.urlCreditos = this.sanitizer.bypassSecurityTrustResourceUrl(environment.urlCreditos)
    this.urlRegistro = this.sanitizer.bypassSecurityTrustResourceUrl(environment.urlRegistro)
    // this._user.refreshIframe( environment.urlCreditos, "ifCreditos");
  }
  
  refrescar( e ){
    console.log(e);
    
    // this.urlCreditos = this.sanitizer.bypassSecurityTrustResourceUrl(environment.urlCreditos)
  }
  
  ngAfterViewInit(): void {
    this._user.refreshIframe( environment.urlPagos, "ifPagos");
    this._user.refreshIframe( environment.urlBecas, "ifBecas");
    this._user.refreshIframe( environment.urlCreditos, "ifCreditos");
    this._user.refreshIframe( environment.urlRegistro, "ifRegistro");
    
  }
}
