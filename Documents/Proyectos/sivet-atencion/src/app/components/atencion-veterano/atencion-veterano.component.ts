import { Component, OnInit, AfterViewInit } from '@angular/core';
import { PrimeNGConfig } from 'primeng/api';
import { AppComponent } from 'src/app/app.component';
import { MenuService } from 'src/app/app.menu.service';
import { PrimeTemplate } from 'primeng/api'; 
import { DomSanitizer } from '@angular/platform-browser';
import { environment } from 'src/environments/environment';
import { UserService } from 'src/app/services/user.service';
@Component({
  selector: 'app-atencion-veterano',
  templateUrl: './atencion-veterano.component.html',
  styleUrls: ['./atencion-veterano.component.scss']
})
export class AtencionVeteranoComponent implements OnInit, AfterViewInit  {
  urlRegistro = this.sanitizer.bypassSecurityTrustResourceUrl(environment.urlRegistro)

  constructor(
    private menuService: MenuService, 
    private primengConfig: PrimeNGConfig, 
    public app: AppComponent, 
    private sanitizer: DomSanitizer,
    private _user : UserService
    ) { }

  ngOnInit(): void {
  }
  
  ngAfterViewInit(): void {
    this.urlRegistro = this.sanitizer.bypassSecurityTrustResourceUrl(environment.urlRegistro)
    setTimeout(() => {
      this._user.refreshIframe( environment.urlRegistro, "ifRegistro" )
  }, 1000);
  }
  
  
  

}
