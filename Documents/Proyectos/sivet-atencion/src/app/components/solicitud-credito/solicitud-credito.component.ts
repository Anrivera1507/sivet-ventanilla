import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { VetVeteranosModel } from 'src/app/models/vet_veteranos';
import { VeteranoService } from 'src/app/services/veterano.service';

@Component({
  selector: 'app-solicitud-credito',
  templateUrl: './solicitud-credito.component.html',
  styleUrls: ['./solicitud-credito.component.scss']
})
export class SolicitudCreditoComponent implements OnInit {

  mensajePrincipal: string = "Estimado(a) solicitante de credito INABVE, la presente solicitud tiene que estar completa con sus datos y los de su codeudor."
  fechaActual: Date = new Date();
  numeroSolicitud: string = "0-00-0000-0000";
  lineaCreditos = [
    {label: 'Minicredito', value: 'Minicredito'},
    {label: 'Consolidación de Deudas', value: 'Consolidación de Deudas'},
    {label: 'Productividad', value: 'Productividad'},
    {label: 'Vivienda', value: 'Vivienda'},
    {label: 'Tierra', value: 'Tierra'},
  ]
  sectores = [
    {label: 'FAES', value: 'FAES'},
    {label: 'FMLN', value: 'FMLN'},
  ]
  decision = [
    {label: 'Si', value: 'Si'},
    {label: 'No', value: 'No'},
  ]
  decisionSeleccionada: any;
  sectorSeleccionado: any;
  lineaCreditoSeleccionada: any;

  veterano: any = {};
  searching         : string 
  filteredVeteran   : any[]
  edad: any;
  edadCodeudor: any;
  codeudores: any[] = [];
  codeudorSeleccionado: any = {};
  compromisosDeudorCodeudor: string = "La información proporcionada en la presente solicitud es verídica y autorizo al Instituto Administrador de los Beneficios de los Veteranos y Excombatientes del Conflicto Armado (INABVE) para que verifique dicha informacion. Adicionalmente, soy conciente y acepto el descuento de forma mensual en concepto de pago de cuota del credito INABVE, a travez de Orden de Descuento hasta la cancelación del crédito";
  compromisosCodeudor: string = "Comprendo mi responsabilidad de pago que se activará en el momento que el deudor fallezca, por lo cual acepto la firma de la orden de descuento para que al momento de tramitar la pensión como Beneficiario se me descuente la cuota mensual hasta la cancelación de este crédito.";
  constructor(public _veterano        : VeteranoService) { }

  ngOnInit(): void {
  }

  selectedVeteram(event){   
    console.log('__veteran_Operat', event);
      
    this.searching = event.dui;
    this.veterano = event;
    this.calcularEdad();
    this.obtenerCodeudores();
    this.sectorSeleccionado = this.sectores.find(x => x.label == this.veterano.sector);
    /* let obj = new Object()
    obj = {carnet:  event.carnet, idVeterano:  event.id,  estadoOperativo: this.estadoOperativo}    
    
    this._configService.obj = obj 
    
    // this._configService.setCarnet(event.noCarnet)
    this.veteranObj =  event  
    this.edad       =  (this.veteranObj.fechaNacimimento==null)?0:moment().diff(this.veteranObj.fechaNacimimento, 'years'); 
    this.sexo       =  (this.veteranObj.sexo == 'M')? 'Masculino' : 'Femenino'  
    this.celular    =  (this.veteranObj.celular1 == '')? this.veteranObj.celular2 : this.veteranObj.celular1
    this.sobrevivenciChild.getOperativeByCarnet(event)
    this.beneficiarioChild.getGrupoFamiliar(event.grupoFamiliar)
    this.beneficiarioChild.getGrupoFamiliar(event.grupoFamiliar) 

    // ENVIANDO AL FORMULARIO DE SONDEO EL OBJ PARA EL ESTADO DEL OPERATIVO
    // Y EL LISTADO DE LAS SOBREVICENCIAS
    this.sondeoChild.operativoVigente(obj, event.sobrevivencias)
    this.sondeoChild.tipoRegistro(event.sector)
     this.sondeoChild.obtenerSondeo(event.sector)
    
    // this.sobrevivenciChild.sobrevivenciaForm.get('noCarnet').setValue(this.veteranObj.noCarnet);   
    // this.sobrevivenciChild.setDefault() 
    this.duiSearch  =  event.dui 
    this.searching = ""  */
    // this.obtenerSondeo()
  }

  calcularEdad() {
    let hoy = moment();
    this.edad = hoy.diff(this.veterano.fechaNacimimento, "years");
    if (this.codeudorSeleccionado != null && this.codeudorSeleccionado.id != null) {
      this.edadCodeudor = hoy.diff(this.codeudorSeleccionado.fechaNacimimento, "years");
    }
  }

  searchVeteran(event){   
    if(event.query && event.query.length>2){  
      let objParam = { busqueda:  event.query , grupoFamiliar: true, sobrevivencia: true, sondeo: true }  
      this._veterano.buscar(objParam).subscribe(   
       resp => {     
         
       let filtered: any[] = [];
       let query = event.query;
       for (let i = 0; i <  resp.data.length; i++) {
         let veteran =  resp.data[i];     
        //  if (veteran.nombreCompleto.toLowerCase().indexOf(query.toLowerCase()) == 0) { 
           filtered.push(veteran);
        //  }
       } 
       this.filteredVeteran = filtered;     
       },  
       (err) => {    
           console.log("Error");   
       },()=>{ 
       })   
    }  
  }

  seleccionarSector(event) {
    console.log(event);
    
  }

  obtenerCodeudores() {
    this._veterano.obtenerCodeudores(this.veterano.id).subscribe((resp: any) => {
      if (resp != null) {
        this.codeudores = resp;
      }
    });
  }

}
